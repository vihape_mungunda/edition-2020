# Programming Competition for High Schools -- Round 1

## Problem 1

Supposing that each programming competition has a minimum of three members. Write that a program that:

* prompts the user to enter the exact size of a team (the minimum is three);
* inputs the weight of each team member;
* prints the average weight of the team.

Example:
* team size: 4
* team members' weight:
	* member 1: 20
	* member 2: 30
	* member 3: 40
	* member 4: 60
* The average weight of your team is: 37.5

## Problem 2

Write a story-line program that can display as output, simple sentences in response to a given input.

Example:
* Input:
	* Enter the person's name: Samuel
	* Enter the person's gender: male
	* Enter the person's year of birth: 2005
	* Enter the person's favourite subject: science
* Output:
	* Samuel is a boy, he is age 15 and could be a future scientist

## Problem 3

A number is said to be a natural number (also called a “counting number”) if it is either a positive integer (e.g., 1, 2, 3, 4, 5) up to infinity or a non-negative integer (0, 1, 2, 3, 4, 5) up to infinity. Write a program that, when given a list of ten natural numbers as input, separates the odd and even numbers in two classes and provides the summation in each class.

Example:
* Enter a list of ten natural numbers: 1,2,3,4,5,6,7,8,9,10
* Even numbers: 2,4,6,8,10 and their sum is 30
* Odd numbers: 1,3,5,7,9 and their sum is 25

## Problem 4

A *mode* is defined as a number that appears the most in any given list of positive numbers. For example, the modal value of the list [2, 3, 4, 5, 3] is “3”. Write a program that takes in any list of positive numbers and prints the modal value thereof. Note that a list of numbers may have more than one modal value. For example, the list 2, 3, 4, 5, 3, 2, 6 has two modal values: 2 and 3.

Example:
* Enter the size of the list: 5
* Please enter the values separated by space: 2 3 4 5 3
* The list of numbers you entered has only one mode, and the value is 3

## Problem 5

The area of a symbolic shape depends on its attributes. For example, the area of a circular shape depends on the radius __r__ (π\*r<sup>2</sup>). The area of a triangular shape depends on the base __b__ and the vertical height __h__ (1/2\*b\*h); the area of a rectangular shape depends on its width __w__ and length __l__ (w\*l).

Write a program that prompts a user to select a shape from a predefined collection of shapes (e.g., rectangular, triangular, circular, etc.). Based on the selection, prompt the user to enter the attributes of the shape and compute the area.
